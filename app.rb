require 'sinatra'
require 'warden'
require 'rack'
require 'json'
require 'httparty'
require 'koala'
require 'omniauth'
require 'omniauth-facebook'
require 'omniauth-twitter'
require 'omniauth-gplus'

use Rack::Session::Cookie, :secret => ENV['SINATRA_SESSION_SECRET_KEY']

use OmniAuth::Builder do
  provider :facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET'], scope: FACEBOOK_SCOPE
  provider :twitter, ENV['TWITTER_APP_ID'], ENV['TWITTER_APP_SECRET']
  provider :gplus, ENV['GPLUS_APP_ID'], ENV['GPLUS_APP_SECRET']
end

# enable :sessions # NOT REQUIRED Rack::Session::Cookie

# Scope defines what permissions that we are asking the user to grant.
# In this example, we are asking for the ability to publish stories
# about using the app, access to what the user likes, and to be able
# to use their pictures.  You should rewrite this scope with whatever
# permissions your app needs.
# See https://developers.facebook.com/docs/reference/api/permissions/
# for a full list of permissions
FACEBOOK_SCOPE = 'user_likes'

unless ENV['FACEBOOK_APP_ID'] && ENV['FACEBOOK_APP_SECRET']
  abort("missing env vars: please set FACEBOOK_APP_ID and FACEBOOK_APP_SECRET with your app credentials")
end

unless ENV['TWITTER_APP_ID'] && ENV['TWITTER_APP_SECRET']
  abort("missing env vars: please set TWITTER_APP_ID and TWITTER_APP_SECRET with your app credentials")
end

unless ENV['GPLUS_APP_ID'] && ENV['GPLUS_APP_SECRET']
  abort("missing env vars: please set GPLUS_APP_ID and GPLUS_APP_SECRET with your app credentials")
end

helpers do
  def current_user
    !session[:uid].nil?
  end
end

get '/login/?' do
  erb :login
end

get '/oauth_fb' do
  redirect '/auth/facebook'
end

get '/oauth_tw' do
  redirect '/auth/twitter'
end

get '/oauth_gp' do
  redirect '/auth/gplus'
end

get '/auth/:provider/callback' do
  provider = params[:provider]
  # code where I create users and sustain them to a database
  session[:uid] = env['omniauth.auth']['uid']
  redirect '/'
end

get '/auth/failure' do
  params[:message]
end

get '/logout/?' do
  env['warden'].logout
  redirect '/'
end


